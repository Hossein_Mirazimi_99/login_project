export default ({redirect,store}) => {
  const TOKEN = localStorage.getItem('__TOKEN__');
  const USER = JSON.parse(localStorage.getItem('__USER__'));
  if(TOKEN) {
    store.commit('auth/SET_USER', USER);
    store.commit('auth/SET_TOKEN', TOKEN);
  } else {
    return redirect('/auth/login');
  }
}