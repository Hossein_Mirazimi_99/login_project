// Validate National Code
function isNationalCode (code) {
  if (code.length !== 10 || /(\d)(\1){9}/.test(code)) return false;

    var sum = 0,
        chars = code.split(''),
        lastDigit,
        remainder;

    for (var i = 0; i < 9; i++) sum += +chars[i] * (10 - i);
    
    remainder = sum % 11;
    lastDigit = remainder < 2 ? remainder : 11 - remainder;

    return +chars[9] === lastDigit;
}

// Validate is Number
function isNum (val) {
  const reg = RegExp(/^\d+$/);
  return reg.test(val);
}

// Export
export {
  isNationalCode,
  isNum
}