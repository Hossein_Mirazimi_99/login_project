// State
export const state = () => ({
  token: null,
  user: null,
});

// Getter
export const getters = {
  isLogin(state) {
    return state.user && state.token;
  },
  userFullName(state) {
    return `${state.user.firstName} ${state.user.lastName}`;
  }
}

// Mutations
export const mutations = {
  SET_USER(state, payload) {
    state.user = payload;
    localStorage.setItem('__USER__', JSON.stringify(payload));
  },
  SET_TOKEN(state, payload) {
    state.token = payload;
    localStorage.setItem('__TOKEN__', payload);
    this.$axios.setToken(payload);
  }
};

// Action
export const actions = {
  async login({commit}, payload) {
    return await this.$axios.post('/login', payload).then(({data}) => {
      console.log(data)
      if(data.status === 1) {
        const {user, access_token, token_type} = data.data;

        // set user
        commit('SET_USER', {
          firstName: user.firstName,
          lastName: user.lastName,
          avatar: user.avatar,
          mobile: user.mobile,
          _id: user.id,
        });
        
        // set token
        commit('SET_TOKEN', `${token_type} ${access_token}`);

        this.$toast.success(data.msg);
        return true;
      } else {
        this.$toast.error(data.msg);
        return false;
      }
    }).catch(({response}) => {
      this.$toast.error(response.data.msg);
      return false;
    });
  }
};